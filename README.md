# The jmf CRUX repo

Hi, this is my personal repository of ports for the source-based Linux
distribution CRUX.

To check the integrity of the files, you will need the public key of this repo.
It can be found here:
* https://crux.nu/keys/jmf.pub

The preferred way is to check out the repo via httpup:

```
# Put this into /etc/ports/jmf.httpup

ROOT_DIR=/usr/ports/jmf
URL=https://cuttle.space/ports/crux-3.5/
```

Alternatively, you can also use git to check out the repo:

```
# Put this into /etc/ports/jmf.git

NAME=jmf
URL=https://codeberg.org/jmf/jmf
BRANCH=3.5
```

If you encounter problems with the ports, please open an issue at
https://codeberg.org/jmf/jmf/issues or drop me a mail.
